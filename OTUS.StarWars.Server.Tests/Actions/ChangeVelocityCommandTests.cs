﻿using NSubstitute;
using OTUS.StarWars.Server.Actions;
using OTUS.StarWars.Server.Domain;
using Xunit;

namespace OTUS.StarWars.Server.Tests.Actions
{
    public class ChangeVelocityCommandTests
    {
        [Theory(DisplayName = "9. Написаны тесты к ChangeVelocityComamnd")]
        [InlineData(2, 1, 1,  0, 1)]
        [InlineData(4, 1, 1, -1, 0)]
        public void ChangeVelocityCommand_Handle_Success(int direct,int velocityX,int velocityY,int expectedX,int expectedY)
        {
            // Arrange
            var command = Substitute.For<IVelocityChangeable>();
            command.Direction.Returns(new Direction(direct));
            command.Velocity.Returns(new Vector(velocityX, velocityY));
            
            var handle = new ChangeVelocityCommand(command);
            
            // Act
            handle.Execute();
            
            // Assert
            var expectedVelocity = new Vector(expectedX, expectedY);
            Assert.Equal(expectedVelocity,command.Velocity);
        }
    }
}