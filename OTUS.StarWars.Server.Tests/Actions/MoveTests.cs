﻿using System;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NSubstitute.ReceivedExtensions;
using OTUS.StarWars.Server.Actions;
using OTUS.StarWars.Server.Domain;
using Xunit;

namespace OTUS.StarWars.Server.Tests.Actions
{
    public class MoveTests
    {
        [Fact(DisplayName = "Для объекта, находящегося в точке (12, 5) и движущегося со скоростью (-7, 3) движение меняет положение объекта на (5, 8)")]
        public void MoveTest1()
        {
            // Arrange
            var moveable = Substitute.For<IMoveable>();
            var move = new MoveCommand(moveable);
            var expected = new Vector(5, 8);
            moveable.Position.Returns(new Vector(12, 5));
            moveable.Velocity.Returns(new Vector(-7, 3));
            
            // Act
            move.Execute();
            // Assert
            Assert.Equal(expected,moveable.Position);
        }
        
        [Fact(DisplayName = "Попытка сдвинуть объект, у которого невозможно прочитать положение в пространстве, приводит к ошибке")]
        public void MoveGetPositionError()
        {
            // Arrange
            var moveable = Substitute.For<IMoveable>();
            var move = new MoveCommand(moveable);
            moveable.Position.Throws(new Exception());
            moveable.Velocity.Returns(new Vector(-7, 3));
            
            // Act
            Action act = () => move.Execute();
            
            // Assert
            Assert.Throws<Exception>(act);
        }
        
        [Fact(DisplayName = "Попытка сдвинуть объект, у которого невозможно прочитать значение мгновенной скорости, приводит к ошибке")]
        public void MoveGetVelocityError()
        {
            // Arrange
            var moveable = Substitute.For<IMoveable>();
            var move = new MoveCommand(moveable);
            moveable.Position.Returns(new Vector(12, 5));
            moveable.Velocity.Throws(new Exception());
            
            // Act
            Action act = () => move.Execute();
            
            // Assert
            Assert.Throws<Exception>(act);
        }
        
        [Fact(DisplayName = "Попытка сдвинуть объект, у которого невозможно изменить положение в пространстве, приводит к ошибке")]
        public void MoveSetPositionError()
        {
            // Arrange
            var moveable = Substitute.For<IMoveable>();
            var move = new MoveCommand(moveable);
            moveable.Position.Returns(new Vector(12, 5));
            moveable.Velocity.Returns(new Vector(-7, 3));
            moveable.When(w => w.Position = Arg.Any<Vector>()).Do(x => throw new Exception());
            // Act
            Action act = () => move.Execute();
            
            // Assert
            Assert.Throws<Exception>(act);
        }
    }
}