﻿using System;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using OTUS.StarWars.Server.Actions;
using OTUS.StarWars.Server.Domain;
using Xunit;

namespace OTUS.StarWars.Server.Tests.Actions
{
    public class RotateTests
    {
        [Theory(DisplayName = "Поворот, успешный тест")]
        [InlineData(1, 2, 3)]
        [InlineData(4, 2, 6)]
        [InlineData(5, 6, 3)]
        [InlineData(2, 3, 5)]
        public void RotateSuccess(int direcition,int angularVelocity, int expectedDirection)
        { 
            // Arrange
            var actual = new Direction(direcition);
            var expected = new Direction(expectedDirection);
            var rotateable = Substitute.For<IRotateable>();
            rotateable.Direction.Returns(actual);
            rotateable.AngularVelocity.Returns(angularVelocity);
            var rotate = new RotateCommand(rotateable);
            // Act
            rotate.Execute();
            // Assert
            Assert.Equal(expected,rotateable.Direction);
        }
        
        [Fact(DisplayName = "Ошибка в GetDirection()")]
        public void RotateGetDirectionError()
        { 
            // Arrange
            var rotateable = Substitute.For<IRotateable>();
            rotateable.Direction.Throws(new Exception());
            rotateable.AngularVelocity.Returns(2);
            var rotate = new RotateCommand(rotateable);
            // Act
            Action act = () => rotate.Execute();
            // Assert
            Assert.Throws<Exception>(act);
        } 
        
        [Fact(DisplayName = "Ошибка в GetAngularVelocity()")]
        public void RotateGetAngularVelocityError()
        { 
            // Arrange
            var rotateable = Substitute.For<IRotateable>();
            rotateable.Direction.Returns(new Direction(1));
            rotateable.AngularVelocity.Throws(new Exception());
            var rotate = new RotateCommand(rotateable);
            // Act
            Action act = () => rotate.Execute();
            // Assert
            Assert.Throws<Exception>(act);
        }
        
        [Fact(DisplayName = "Ошибка в SetDirection()")]
        public void RotateSetDirectionError()
        { 
            // Arrange
            var rotateable = Substitute.For<IRotateable>();
            rotateable.Direction.Returns(new Direction(1));
            rotateable.AngularVelocity.Returns(2);
            rotateable.When(w => w.Direction =  Arg.Any<Direction>()).Do(x => throw new Exception());
            var rotate = new RotateCommand(rotateable);
            // Act
            Action act = () => rotate.Execute();
            // Assert
            Assert.Throws<Exception>(act);
        }
    }
}