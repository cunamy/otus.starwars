﻿using System;
using NSubstitute;
using OTUS.StarWars.Server.Actions;
using OTUS.StarWars.Server.Commands;
using OTUS.StarWars.Server.Domain;
using Xunit;

namespace OTUS.StarWars.Server.Tests.Commands
{
    public class BurnFuelCommandTests
    {
        [Fact(DisplayName = "5. Написаны тесты к BurnFuelComamnd ")]
        void CheckFuelCommand_Success()
        {
            // Arrange
            var ship = Substitute.For<IFuelable>();
            ship.Fuel.Returns(11);
            ship.FuelBurn.Returns(5);
            var command = new BurnFuelCommand(ship);
            double expected = 6;
            // Act
            command.Execute();
            
            // Assert
            Assert.True(Math.Abs(expected-ship.Fuel)<Game.Epsilon);
        }
    }
}