﻿using System;
using NSubstitute;
using OTUS.StarWars.Server.Actions;
using OTUS.StarWars.Server.Commands;
using OTUS.StarWars.Server.Exceptions;
using Xunit;

namespace OTUS.StarWars.Server.Tests.Commands
{
    public class CheckFuelCommandTests
    {
        [Fact(DisplayName = "1. Реализовать класс CheckFuelComamnd и тесты к нему, успешное выполнение")]
        void CheckFuelCommand_Success()
        {
            // Arrange
            var ship = Substitute.For<IFuelable>();
            ship.Fuel.Returns(10);
            ship.FuelBurn.Returns(5);
            var command = new CheckFuelCommand(ship);
            
            // Act
            Action act = () => command.Execute();
            
            // Assert
            var exception = Record.Exception(()=>act);
            Assert.Null(exception);
        }
        
        [Fact(DisplayName = "1. Реализовать класс CheckFuelComamnd и тесты к нему, нехватка топлива")]
        void CheckFuelCommand_Fault()
        {
            // Arrange
            var ship = Substitute.For<IFuelable>();
            ship.Fuel.Returns(10);
            ship.FuelBurn.Returns(11);
            var command = new CheckFuelCommand(ship);
            
            // Act
            Action act = () => command.Execute();
            
            // Assert
            FuelNotEnoughException exception = Assert.Throws<FuelNotEnoughException>(act);
            Assert.Equal("Недостаточно топлива!",exception.Message);
        }
        
        [Fact(DisplayName = "1. Реализовать класс CheckFuelComamnd и тесты к нему, топлива впритык")]
        void CheckFuelCommand_Equal()
        {
            // Arrange
            var ship = Substitute.For<IFuelable>();
            ship.Fuel.Returns(10);
            ship.FuelBurn.Returns(10);
            var command = new CheckFuelCommand(ship);
            
            // Act
            Action act = () => command.Execute();
            
            // Assert
            var exception = Record.Exception(()=>act);
            Assert.Null(exception);
        }
    }
}