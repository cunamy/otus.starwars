﻿using System;
using NSubstitute;
using OTUS.StarWars.Server.Commands;
using OTUS.StarWars.Server.Domain;
using Xunit;

namespace OTUS.StarWars.Server.Tests.Commands
{
    public class LineFuelBurnCommandTests
    {
        [Theory(DisplayName = "6. Реализована макрокоманда движения по прямой с расходом топлива и тесты к ней ")]
        [InlineData(100, 10,0,0,1,1,90,1,1)]
        [InlineData(100, 50,1,1,-1,-1,50,0,0)]
        public void LineFuelBurnCommand_Success(int fuel, int fuelBurn,int positionX, int positionY,int velocityX,int velocityY, int expectedFuel,int expextedPositionX, int expextedPositionY)
        {
            // Arrange
            var uniObject = Substitute.For<IUniObject>();
            uniObject.Fuel.Returns(fuel);
            uniObject.FuelBurn.Returns(fuelBurn);
            var position = new Vector(positionX, positionY);
            var velocity = new Vector(velocityX, velocityY);
            uniObject.Position.Returns(position);
            uniObject.Velocity.Returns(velocity);
            
            var handler = new LineFuelBurnCommand(uniObject);
            // Act
            handler.Execute();
            // Assert 
            var expextedPosition = new Vector(expextedPositionX, expextedPositionY);
            Assert.Equal(expectedFuel,uniObject.Fuel);
            Assert.Equal(expextedPosition,uniObject.Position);
        }
    }
}