﻿using System;
using log4net;
using NSubstitute;
using OTUS.StarWars.Server.Commands;
using Xunit;

namespace OTUS.StarWars.Server.Tests.Commands
{
    public class LogCommandTest
    {
        [Fact(DisplayName = "Успешный вызов команды логирования")]
        void LogCommand_Success()
        {
            // Arrange
            var log = Substitute.For<ILog>();
            var ex = new ArgumentException();
            var command = new LogCommand(log,ex);
            
            // Act
            command.Execute();
            
            // Assert
            log.Received().Error(Arg.Is<Exception>(e => e == ex));
        }
    }
}