﻿using System;
using System.Collections.Generic;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NSubstitute.ReceivedExtensions;
using OTUS.StarWars.Server.Commands;
using OTUS.StarWars.Server.Exceptions;
using Xunit;

namespace OTUS.StarWars.Server.Tests.Commands
{
    public class MacroCommandTests
    {
        [Fact(DisplayName = "7. Написаны тесты к MacroComamnd . Успешное выполнение")]
        public void MacroCommand_Success()
        {
            // Arrange
            var command1 = Substitute.For<ICommand>();
            var command2 = Substitute.For<ICommand>();
            var command3 = Substitute.For<ICommand>();
            List<ICommand> commands = new List<ICommand>
            {
                command1,
                command2,
                command3
            };
            var handler = new MacroCommand(commands);
            
            // Act
            handler.Execute();
            
            // Assert
            command1.Received().Execute();
            command2.Received().Execute();
            command3.Received().Execute();
        }
        
        [Fact(DisplayName = "7. Написаны тесты к MacroComamnd . Ошибка в команде1")]
        public void MacroCommand_Command1_Error()
        {
            // Arrange
            var command1 = Substitute.For<ICommand>();
            command1.When(w => w.Execute())
                .Do(d => throw new CommandException("ddd"));
            var command2 = Substitute.For<ICommand>();
            var command3 = Substitute.For<ICommand>();
            List<ICommand> commands = new List<ICommand>
            {
                command1,
                command2,
                command3
            };
            var handler = new MacroCommand(commands);
            
            // Act
            Action act = () => handler.Execute();
            
            // Assert
            Assert.Throws<CommandException>(act);
            command1.Received().Execute();
            command2.DidNotReceive().Execute();
            command3.DidNotReceive().Execute();
        }
        
        [Fact(DisplayName = "7. Написаны тесты к MacroComamnd . Ошибка в команде2")]
        public void MacroCommand_Command2_Error()
        {
            // Arrange
            var command1 = Substitute.For<ICommand>();
            var command2 = Substitute.For<ICommand>();
            command2.When(w => w.Execute())
                .Do(d => throw new CommandException("ddd"));
            var command3 = Substitute.For<ICommand>();
            List<ICommand> commands = new List<ICommand>
            {
                command1,
                command2,
                command3
            };
            var handler = new MacroCommand(commands);
            
            // Act
            Action act = () => handler.Execute();
            
            // Assert
            Assert.Throws<CommandException>(act);
            command1.Received().Execute();
            command2.Received().Execute();
            command3.DidNotReceive().Execute();
        }
    }
}