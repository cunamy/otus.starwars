﻿using System.Collections.Generic;
using System.Linq;
using NSubstitute;
using OTUS.StarWars.Server.Commands;
using Xunit;

namespace OTUS.StarWars.Server.Tests.Commands
{
    public class RetryCommandTests
    {
        [Fact(DisplayName = "Успешный вызов команды повторной обработки")]
        void RetryCommand_Success()
        {
            // Arrange
            Queue<ICommand> queue = new Queue<ICommand>();
            var command = Substitute.For<ICommand>();
            var act = new RetryCommand(queue,command);
            
            // Act
            act.Execute();
            
            // Assert
            Assert.Contains(queue.AsEnumerable(), q => q == command);
        }
    }
}