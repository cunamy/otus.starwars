﻿using NSubstitute;
using OTUS.StarWars.Server.Commands;
using OTUS.StarWars.Server.Domain;
using Xunit;

namespace OTUS.StarWars.Server.Tests.Commands
{
    public class RotateChangeVelocityCommandTests
    {
        [Theory(DisplayName = "10. Реализована команда поворота, которая еще и меняет вектор мгновенной скорости")]
        [InlineData(1, 2, 1, 1, 3, -1, 1)]
        [InlineData(3, 5, 2, 3, 0, 3, 0)]
        public void RotateChangeVelocityCommand_Success(int direction,int angularVelocity,int velocityX,int velocityY,int expectedDirection,int expectedVelocityX, int expectedVelocityY)
        {
            // Arrange
            var velocity = new Vector(velocityX, velocityY);
            var uniObject = Substitute.For<IUniObject>();
            uniObject.Direction.Returns(new Direction(direction));
            uniObject.AngularVelocity.Returns(angularVelocity);
            uniObject.Velocity.Returns(velocity);
            
            var handler = new RotateChangeVelocityCommand(uniObject);
            
            // Act
            handler.Execute();
            
            // Assert
            var expextedVelocity = new Vector(expectedVelocityX, expectedVelocityY);
            Assert.Equal(expextedVelocity, uniObject.Velocity);
            Assert.Equal(expectedDirection, uniObject.Direction.Direct);
        }
        
    }
}