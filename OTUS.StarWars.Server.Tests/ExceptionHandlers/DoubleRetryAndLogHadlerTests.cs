﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NSubstitute;
using OTUS.StarWars.Server.Commands;
using OTUS.StarWars.Server.ExceptionHandlers;
using Xunit;

namespace OTUS.StarWars.Server.Tests.ExceptionHandlers
{
    public class DoubleRetryAndLogHadlerTests
    {
        [Fact(DisplayName = "9. При первом выбросе исключения повторить команду")]
        public void FirstException()
        {
            // Arrange
            var command = Substitute.For<ICommand>();
            var exception = Substitute.For<Exception>();
            var log = Substitute.For<ILog>();
            Queue<ICommand> queue = new Queue<ICommand>();
            
            var exceptionHandler = new DoubleRetryAndLogHadler(queue,log);
            
            // Act
            exceptionHandler.Handle(command,exception);

            // Assert
            Assert.Contains(queue.AsEnumerable(), (c) => c is RetryCommand);
        }
        
        [Fact(DisplayName = "9. Повторить два раза")]
        public void SecondException()
        {
            // Arrange
            Queue<ICommand> queue = new Queue<ICommand>();
            var prevCommand = Substitute.For<ICommand>();
            var command = Substitute.For<RetryCommand>(queue,prevCommand);
            var exception = Substitute.For<Exception>();
            var log = Substitute.For<ILog>();
            
            
            var exceptionHandler = new DoubleRetryAndLogHadler(queue,log);
            
            // Act
            exceptionHandler.Handle(command,exception);

            // Assert
            Assert.Contains(queue.AsEnumerable(), (c) => c is SecondRetryCommand);
        }
        
        [Fact(DisplayName = "9. потом записать в лог")]
        public void ThirdException()
        {
            // Arrange
            var exception = Substitute.For<Exception>();
            var log = Substitute.For<ILog>();
            Queue<ICommand> queue = new Queue<ICommand>();
            var prevCommand = Substitute.For<ICommand>();
            var command = Substitute.For<SecondRetryCommand>(queue,prevCommand);
            
            var exceptionHandler = new DoubleRetryAndLogHadler(queue,log);
            
            // Act
            exceptionHandler.Handle(command,exception);

            // Assert
            Assert.Contains(queue.AsEnumerable(), (c) => c is LogCommand);
        }
    }
}