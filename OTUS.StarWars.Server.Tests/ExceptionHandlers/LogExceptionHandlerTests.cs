﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NSubstitute;
using OTUS.StarWars.Server.Commands;
using OTUS.StarWars.Server.ExceptionHandlers;
using Xunit;

namespace OTUS.StarWars.Server.Tests.ExceptionHandlers
{
    public class LogExceptionHandlerTests
    {
        [Fact(DisplayName = "Успешное выполнение хандлера постановки команды логирования в очередь")]
        public void Handle_Success()
        {
            // Arrange
            var log = Substitute.For<ILog>();
            var command = Substitute.For<ICommand>();
            var exception = Substitute.For<Exception>();
            Queue<ICommand> queue = new Queue<ICommand>();
            var exceptionHandler = new LogExceptionHandler(log,queue);
            
            // Act
            exceptionHandler.Handle(command,exception);

            // Assert
            Assert.Contains(queue.AsEnumerable(), (c) => c is LogCommand);
        }
    }
}