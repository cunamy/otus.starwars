﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NSubstitute;
using OTUS.StarWars.Server.Commands;
using OTUS.StarWars.Server.ExceptionHandlers;
using Xunit;

namespace OTUS.StarWars.Server.Tests.ExceptionHandlers
{
    public class RetryAndLogHadlerTests
    {
        [Fact(DisplayName = "8. При первом выбросе исключения повторить команду")]
        public void FirstException()
        {
            // Arrange
            var command = Substitute.For<ICommand>();
            var exception = Substitute.For<Exception>();
            var log = Substitute.For<ILog>();
            Queue<ICommand> queue = new Queue<ICommand>();
            
            var exceptionHandler = new RetryAndLogHadler(queue,log);
            
            // Act
            exceptionHandler.Handle(command,exception);

            // Assert
            Assert.Contains(queue.AsEnumerable(), (c) => c is RetryCommand);
        }

        [Fact(DisplayName = "8. При повторном выбросе исключения записать информацию в лог")]
        public void SecondException()
        {
            // Arrange
            var exception = Substitute.For<Exception>();
            var log = Substitute.For<ILog>();
            Queue<ICommand> queue = new Queue<ICommand>();
            var prevCommand = Substitute.For<ICommand>();
            var command = Substitute.For<RetryCommand>(queue,prevCommand);
            
            var exceptionHandler = new RetryAndLogHadler(queue,log);
            
            // Act
            exceptionHandler.Handle(command,exception);

            // Assert
            Assert.Contains(queue.AsEnumerable(), (c) => c is LogCommand);
        }
    }
}