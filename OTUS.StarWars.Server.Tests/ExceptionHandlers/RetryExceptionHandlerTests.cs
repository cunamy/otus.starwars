﻿using System;
using System.Collections.Generic;
using System.Linq;
using NSubstitute;
using OTUS.StarWars.Server.Commands;
using OTUS.StarWars.Server.ExceptionHandlers;
using Xunit;

namespace OTUS.StarWars.Server.Tests.ExceptionHandlers
{
    public class RetryExceptionHandlerTests
    {
        [Fact(DisplayName = "7. Реализовать обработчик исключения, который ставит в очередь Команду - повторитель команды, выбросившей исключение")]
        public void Handle_Success()
        {
            // Arrange
            var command = Substitute.For<ICommand>();
            var exception = Substitute.For<Exception>();
            Queue<ICommand> queue = new Queue<ICommand>();
            var exceptionHandler = new RetryExceptionHandler(queue);
            
            // Act
            exceptionHandler.Handle(command,exception);

            // Assert
            Assert.Contains(queue.AsEnumerable(), (c) => c is RetryCommand);
        }
    }
}