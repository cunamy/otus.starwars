﻿using System;
using System.Threading;
using System.Threading.Tasks;
using OTUS.StarWars.Server.Commands;
using OTUS.StarWars.Server.Exceptions;
using OTUS.StarWars.Server.Infrastructure;
using Xunit;

namespace OTUS.StarWars.Server.Tests.Infrastructure
{
    public class IoCTests
    {
        
        [Fact(DisplayName = "4.Реализованы модульные тесты. Наличие стандартных обработчиков")]
        public void IoC_System()
        {
            // Arrange

            // Act
            
            // Отдельный поток
            Task<int> task1 = Task<int>.Factory.StartNew(() =>
            {
                IoC.Resolve<ICommand>(PredefinedIoCFunctions.Register, "value", (Func<object[], object>)(args => 9)).Execute();
                return IoC.Resolve<int>("value");
            });
            int result1 = task1.Result;
            
            // Текущий поток
            Action act = () => IoC.Resolve<int>("value"); 
            
            // Assert
            Assert.Equal(9,result1);
            Assert.Throws<DependencyNotRegistered>(act);
        }
        
        [Fact(DisplayName = "5.Реализованы многопоточные тесты")]
        public void IoC_MultyThreads()
        {
            // Arrange

            // Act
            
            // Отдельный поток
            Task<int> task1 = Task<int>.Factory.StartNew(() =>
            {
                IoC.Resolve<ICommand>(PredefinedIoCFunctions.Register, "value", (Func<object[], object>)(args => 9)).Execute();
                return IoC.Resolve<int>("value");
            });
            int result1 = task1.Result;
            
            // Текущий поток
            Action act = () => IoC.Resolve<int>("value"); 
            
            // Assert
            Assert.Equal(9,result1);
            Assert.Throws<DependencyNotRegistered>(act);
        }
    }
}