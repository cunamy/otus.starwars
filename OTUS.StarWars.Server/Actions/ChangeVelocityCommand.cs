﻿using System;
using OTUS.StarWars.Server.Commands;
using OTUS.StarWars.Server.Domain;

namespace OTUS.StarWars.Server.Actions
{
    public class ChangeVelocityCommand : ICommand
    {
        private readonly IVelocityChangeable _velocityChangeable;

        public ChangeVelocityCommand(IVelocityChangeable velocityChangeable)
        {
            _velocityChangeable = velocityChangeable;
        }

        public void Execute()
        {
            double angle = 2 * Math.PI * _velocityChangeable.Direction.Direct /
                           _velocityChangeable.Direction.DirectionNumber;
            
            _velocityChangeable.Velocity = new Vector(
                (int) (_velocityChangeable.Velocity.Module() * Math.Cos(angle)),
                (int) (_velocityChangeable.Velocity.Module() * Math.Sin(angle))
                );
        }
    }
}