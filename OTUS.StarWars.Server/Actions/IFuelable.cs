﻿namespace OTUS.StarWars.Server.Actions
{
    public interface IFuelable
    {
        
        int Fuel { get; set; }
        int FuelBurn { get; set; }
    }
}