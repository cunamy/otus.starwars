﻿using OTUS.StarWars.Server.Domain;

namespace OTUS.StarWars.Server.Actions
{
    public interface IMoveable
    {
        Vector Position { get; set; }
        Vector Velocity { get; }
    }
}