﻿using OTUS.StarWars.Server.Domain;

namespace OTUS.StarWars.Server.Actions
{
    public interface IRotateable
    {
        Direction Direction { get; set; }
        int AngularVelocity { get; }
    }
}