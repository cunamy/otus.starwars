﻿using OTUS.StarWars.Server.Domain;

namespace OTUS.StarWars.Server.Actions
{
    public interface IVelocityChangeable
    {
        Direction Direction { get; }
        Vector Velocity { get; set; }
        int AmountOfDirections { get; }
    }
}