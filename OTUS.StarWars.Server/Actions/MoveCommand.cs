﻿using System.Numerics;
using OTUS.StarWars.Server.Commands;

namespace OTUS.StarWars.Server.Actions
{
    public class MoveCommand : ICommand
    {
        private readonly IMoveable _moveable;

        public MoveCommand(IMoveable moveable)
        {
            _moveable = moveable;
        }

        public void Execute()
        {
            _moveable.Position += _moveable.Velocity;
        }
    }
}