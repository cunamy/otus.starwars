﻿using OTUS.StarWars.Server.Commands;

namespace OTUS.StarWars.Server.Actions
{
    public class RotateCommand : ICommand
    {
        private readonly IRotateable _rotateable;

        public RotateCommand(IRotateable rotateable)
        {
            _rotateable = rotateable;
        }

        public void Execute()
        {
            _rotateable.Direction =(_rotateable.Direction.Next(_rotateable.AngularVelocity));
        }
    }
}