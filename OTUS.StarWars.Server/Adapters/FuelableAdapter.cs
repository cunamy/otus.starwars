﻿using OTUS.StarWars.Server.Actions;
using OTUS.StarWars.Server.Domain;

namespace OTUS.StarWars.Server.Adapters
{
    public class FuelableAdapter : IFuelable
    {
        private readonly IUniObject _uniObject;

        public FuelableAdapter(IUniObject uniObject)
        {
            _uniObject = uniObject;
        }

        public int Fuel
        {
            get => _uniObject.Fuel;
            set => _uniObject.Fuel = value;
        }

        public int FuelBurn
        {
            get => _uniObject.FuelBurn;
            set => _uniObject.FuelBurn = value;
        }
    }
}