﻿using OTUS.StarWars.Server.Actions;
using OTUS.StarWars.Server.Domain;

namespace OTUS.StarWars.Server.Adapters
{
    public class MoveableAdapter : IMoveable
    {
        private readonly IUniObject _uniObject;

        public MoveableAdapter(IUniObject uniObject)
        {
            _uniObject = uniObject;
        }

        public Vector Position
        {
            get => _uniObject.Position;
            set => _uniObject.Position = value;
        }

        public Vector Velocity => _uniObject.Velocity;
    }
}