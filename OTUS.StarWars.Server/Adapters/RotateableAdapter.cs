﻿using System;
using OTUS.StarWars.Server.Actions;
using OTUS.StarWars.Server.Domain;

namespace OTUS.StarWars.Server.Adapters
{
    public class RotateableAdapter : IRotateable
    {
        private readonly IUniObject _uniObject;
        public RotateableAdapter(IUniObject uniObject)
        {
            _uniObject = uniObject;
        }
        public Direction Direction
        {
            get => _uniObject.Direction;
            set => _uniObject.Direction = value;
        }

        public int AngularVelocity => _uniObject.AngularVelocity;
    }
}