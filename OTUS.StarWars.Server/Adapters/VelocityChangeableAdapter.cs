﻿using OTUS.StarWars.Server.Actions;
using OTUS.StarWars.Server.Domain;

namespace OTUS.StarWars.Server.Adapters
{
    public class VelocityChangeableAdapter : IVelocityChangeable
    {
        private readonly IUniObject _uniObject;

        public VelocityChangeableAdapter(IUniObject uniObject)
        {
            _uniObject = uniObject;
        }

        public Direction Direction => _uniObject.Direction;

        public Vector Velocity
        {
            get => _uniObject.Velocity;
            set => _uniObject.Velocity = value;
        }

        public int AmountOfDirections => _uniObject.Direction.DirectionNumber;
    }
}