﻿using OTUS.StarWars.Server.Actions;

namespace OTUS.StarWars.Server.Commands
{
    public class BurnFuelCommand : ICommand
    {
        private readonly IFuelable _fuelable;

        public BurnFuelCommand(IFuelable fuelable)
        {
            _fuelable = fuelable;
        }

        public void Execute()
        {
            _fuelable.Fuel -= _fuelable.FuelBurn;
        }
    }
}