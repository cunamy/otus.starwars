﻿using System;
using OTUS.StarWars.Server.Actions;
using OTUS.StarWars.Server.Domain;
using OTUS.StarWars.Server.Exceptions;

namespace OTUS.StarWars.Server.Commands
{
    public class CheckFuelCommand : ICommand
    {
        private readonly IFuelable _fuelable;

        public CheckFuelCommand(IFuelable fuelable)
        {
            _fuelable = fuelable;
        }

        public void Execute()
        {
            if (_fuelable.Fuel - _fuelable.FuelBurn < -Game.Epsilon)
                throw new FuelNotEnoughException("Недостаточно топлива!");
        }
    }
}