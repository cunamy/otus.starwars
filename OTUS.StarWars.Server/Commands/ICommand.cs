﻿namespace OTUS.StarWars.Server.Commands
{
    public interface ICommand
    {
        void Execute();
    }
}