﻿using System;
using System.Threading;
using OTUS.StarWars.Server.Infrastructure;

namespace OTUS.StarWars.Server.Commands
{
    public class IoCRegisterCommand : ICommand
    {
        private readonly string _key;
        private readonly Func<object[], object> _func;

        public IoCRegisterCommand(string key, Func<object[], object> func)
        {
            _key = key;
            _func = func;
        }

        public void Execute()
        {
            var currentScope = IoC.Resolve<Scope>(PredefinedScopeFunctions.Current); 
            currentScope.Set(_key,_func);
        }
    }
}