﻿using System.Collections.Generic;
using OTUS.StarWars.Server.Actions;
using OTUS.StarWars.Server.Adapters;
using OTUS.StarWars.Server.Domain;

namespace OTUS.StarWars.Server.Commands
{
    public class LineFuelBurnCommand : ICommand
    {
        private readonly IUniObject _uniObject;

        public LineFuelBurnCommand(IUniObject uniObject)
        {
            _uniObject = uniObject;
        }

        public void Execute()
        {
            var commands = new List<ICommand>
            {
                new CheckFuelCommand(new FuelableAdapter(_uniObject)),
                new MoveCommand(new MoveableAdapter(_uniObject)),
                new BurnFuelCommand(new FuelableAdapter(_uniObject))
            };
            var macroCommand = new MacroCommand(commands);
            macroCommand.Execute();

        }
    }
}