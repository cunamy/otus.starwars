﻿using System;
using log4net;

namespace OTUS.StarWars.Server.Commands
{
    public class LogCommand : ICommand
    {
        private readonly ILog _log;
        private readonly Exception _exception; 

        public LogCommand(ILog log, Exception exception)
        {
            _log = log;
            _exception = exception;
        }
        public void Execute()
        {
            _log.Error(_exception);
        }
    }
}