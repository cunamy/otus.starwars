﻿using System;
using System.Collections.Generic;
using OTUS.StarWars.Server.Exceptions;

namespace OTUS.StarWars.Server.Commands
{
    public class MacroCommand : ICommand
    {
        private readonly List<ICommand> _commands;

        public MacroCommand(List<ICommand> commands)
        {
            _commands = commands;
        }

        public void Execute()
        {
            try
            {
                foreach (ICommand c in _commands)
                    c.Execute();
            }
            catch (Exception ex)
            {
                throw new CommandException(ex.ToString());
            }
        }
    }
}