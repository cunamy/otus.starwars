﻿using System;
using System.Collections.Generic;
using OTUS.StarWars.Server.ExceptionHandlers;

namespace OTUS.StarWars.Server.Commands
{
    public class QueueHandler : ICommand
    {
        private readonly Queue<ICommand> _queue;
        private readonly ExceptionHandler _exceptionHandler;
        
        public QueueHandler(Queue<ICommand> queue, ExceptionHandler exceptionHandler)
        {
            _queue = queue;
            _exceptionHandler = exceptionHandler;
        }
        public void Execute()
        {
            while (true)
            {
                var comand = _queue.Dequeue();
                try
                {
                    comand.Execute();
                }
                catch (Exception ex)
                {
                    _exceptionHandler.Handle(comand,ex);
                }
            }
        }
        
        
    }
}