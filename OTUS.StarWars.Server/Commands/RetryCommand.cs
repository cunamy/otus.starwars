﻿using System.Collections.Generic;

namespace OTUS.StarWars.Server.Commands
{
    public class RetryCommand : ICommand
    {
        private readonly ICommand _command;
        private readonly Queue<ICommand> _queue;

        public RetryCommand(Queue<ICommand> queue, ICommand command)
        {
            _command = command;
            _queue = queue;
        }

        public void Execute()
        {
            _queue.Enqueue(_command);
        }
    }
}