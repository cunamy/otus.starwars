﻿using System.Collections.Generic;
using OTUS.StarWars.Server.Actions;
using OTUS.StarWars.Server.Adapters;
using OTUS.StarWars.Server.Domain;

namespace OTUS.StarWars.Server.Commands
{
    public class RotateChangeVelocityCommand: ICommand
    {
        private readonly IUniObject _uniObject;

        public RotateChangeVelocityCommand(IUniObject uniObject)
        {
            _uniObject = uniObject;
        }

        public void Execute()
        {
            var commands = new List<ICommand>
            {
                new RotateCommand(new RotateableAdapter(_uniObject)),
                new ChangeVelocityCommand(new VelocityChangeableAdapter(_uniObject))
            };
            var macroCommand = new MacroCommand(commands);
            macroCommand.Execute();
        }
    }
}