﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OTUS.StarWars.Server.Infrastructure;

namespace OTUS.StarWars.Server.Commands
{
    public class ScopeCreateAndSetNewCommand : ICommand
    {
        

        public void Execute()
        {
            var currentScope = IoC.Resolve<Scope>(PredefinedScopeFunctions.Current);
            var newScope = new Scope(currentScope);
            IoC.Resolve<ICommand>(PredefinedScopeFunctions.SetCurrent, newScope);
        }
    }
}