﻿using System.Collections.Generic;
using System.Threading;
using OTUS.StarWars.Server.Infrastructure;

namespace OTUS.StarWars.Server.Commands
{
    public class ScopesSetCurrentCommand : ICommand
    {
        private ThreadLocal<Scope> _currentScope;
        private readonly Scope _scope;

        public ScopesSetCurrentCommand(ThreadLocal<Scope> currentScope, Scope scope)
        {
            _scope = scope;
            _currentScope = currentScope;
        }

        public void Execute()
        {
            _currentScope.Value = _scope;
        }
    }
}