﻿using System.Collections.Generic;

namespace OTUS.StarWars.Server.Commands
{
    public class SecondRetryCommand : RetryCommand
    {
        public SecondRetryCommand(Queue<ICommand> queue, ICommand command) : base(queue, command)
        {
        }
    }
}