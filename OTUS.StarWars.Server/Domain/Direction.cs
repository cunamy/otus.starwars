﻿using System;

namespace OTUS.StarWars.Server.Domain
{
    public class Direction
    {
        public int Direct { get; set; }

        public readonly int DirectionNumber = 8;

        public Direction(int direct)
        {
            Direct = direct;
        }
        
        public Direction Next(int angularVelocity)
        {
            return new Direction((Direct + angularVelocity) % DirectionNumber);
        }

        public override bool Equals(object? obj)
        {
            var direction = obj as Direction;
            return direction?.Direct == Direct;
        }


        public override int GetHashCode()
        {
            return HashCode.Combine(Direct);
        }
    }
}