﻿using OTUS.StarWars.Server.Actions;

namespace OTUS.StarWars.Server.Domain
{
    public interface IUniObject
    {
        Direction Direction { get; set; }
        int AngularVelocity { get; }
        int Fuel { get; set; }
        int FuelBurn { get; set;}
        Vector Position { get; set;}
        Vector Velocity { get; set;}
    }
}