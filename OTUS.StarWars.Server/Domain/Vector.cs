﻿using System;

namespace OTUS.StarWars.Server.Domain
{
    public class Vector
    {
        public Vector(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; set; }
        public int Y { get; set; }

        public override bool Equals(object? obj)
        {
            return Equals(obj as Vector);
        }

        protected bool Equals(Vector other)
        {
            return X == other.X && Y == other.Y;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(X, Y);
        }

        public static Vector operator + (Vector vector1, Vector vector2)
        {
            return new Vector(vector1.X + vector2.X, vector1.Y + vector2.Y);
        }
        
        public static Vector operator - (Vector vector1, Vector vector2)
        {
            return new Vector(vector2.X - vector1.X, vector2.Y - vector1.Y);
        }
        
        public static bool operator == (Vector vector1, Vector vector2)
        {
            return vector1 != null && vector2 != null && vector1.X == vector2.X && vector1.Y == vector2.Y; 
        }

        public static bool operator !=(Vector vector1, Vector vector2)
        {
            return !(vector1 == vector2);
        }

        public double Module()
        {
            return Math.Sqrt(X * X + Y * Y);
        }
    }
}