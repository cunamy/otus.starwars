﻿using System;
using System.Collections.Generic;
using log4net;
using OTUS.StarWars.Server.Commands;

namespace OTUS.StarWars.Server.ExceptionHandlers
{
    public class DoubleRetryAndLogHadler : IExceptionHandler
    {
        private readonly Queue<ICommand> _queue;
        private readonly ILog _log;

        public DoubleRetryAndLogHadler(Queue<ICommand> queue, ILog log)
        {
            _queue = queue;
            _log = log;
        }

        public void Handle(ICommand command, Exception exception)
        {
            if (command is SecondRetryCommand)
            {
                var logCommand = new LogCommand(_log, exception);
                _queue.Enqueue(logCommand);
                return;
            }
            
            if (command is RetryCommand)
            {
                var secondRetry = new SecondRetryCommand(_queue, command);
                _queue.Enqueue(secondRetry);
                return;
            }
            
            var retryCommand = new RetryCommand(_queue, command);
            _queue.Enqueue(retryCommand);
        }
    }
}