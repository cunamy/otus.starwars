﻿using System;
using OTUS.StarWars.Server.Commands;

namespace OTUS.StarWars.Server.ExceptionHandlers
{
    public interface IExceptionHandler
    {
        void Handle(ICommand command, Exception exception);
    }
}