﻿using System;
using System.Collections.Generic;
using log4net;
using OTUS.StarWars.Server.Commands;

namespace OTUS.StarWars.Server.ExceptionHandlers
{
    public class LogExceptionHandler : IExceptionHandler
    {
        private readonly Queue<ICommand> _queue;
        private readonly ILog _log;

        public LogExceptionHandler(ILog log,Queue<ICommand> queue)
        {
            _queue = queue;
            _log = log;
        }

        public void Handle(ICommand command, Exception exception)
        {
            var logCommand = new LogCommand(_log, exception);
            _queue.Enqueue(logCommand);
        }
    }
}