﻿using System;
using System.Collections.Generic;
using log4net;
using OTUS.StarWars.Server.Commands;

namespace OTUS.StarWars.Server.ExceptionHandlers
{
    public class RetryAndLogHadler : IExceptionHandler
    {
        private readonly Queue<ICommand> _queue;
        private readonly ILog _log;

        public RetryAndLogHadler(Queue<ICommand> queue, ILog log)
        {
            _queue = queue;
            _log = log;
        }

        public void Handle(ICommand command, Exception exception)
        {
            if (command is RetryCommand)
            {
                var logCommand = new LogCommand(_log, exception);
                _queue.Enqueue(logCommand);
                return;
            }
            
            var retryCommand = new RetryCommand(_queue, command);
            _queue.Enqueue(retryCommand);
        }
    }
}