﻿using System;

namespace OTUS.StarWars.Server.Exceptions
{
    public class CommandException : Exception
    {
        public CommandException(string message) : base(message)
        {
            
        }
    }
}