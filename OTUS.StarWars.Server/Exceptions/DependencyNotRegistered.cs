﻿using System;

namespace OTUS.StarWars.Server.Exceptions
{
    public class DependencyNotRegistered  : Exception
    {
        public DependencyNotRegistered(string message) : base(message)
        {
            
        }
    }
}