﻿using System;

namespace OTUS.StarWars.Server.Exceptions
{
    public class FuelNotEnoughException : Exception
    {
        public FuelNotEnoughException(string message) : base(message)
        {
            
        }
    }
}