﻿using System;
using System.Collections.Generic;
using System.Threading;
using OTUS.StarWars.Server.Commands;

namespace OTUS.StarWars.Server.Infrastructure
{
    public static class IoC
    {
        private static ThreadLocal<Scope> Scope { get; set; }

        static IoC()
        {
            InitScopes();
        }

        static void InitScopes()
        {
            Scope = new ThreadLocal<Scope>(() =>
            {
                var currentScope = new Scope(null);
                RegisterSystemFunction(currentScope);
                return currentScope;
            });
            
        }
        static void RegisterSystemFunction(Scope currentScope)
        {
            currentScope.Set(PredefinedScopeFunctions.Current,(arg) => Scope.Value);
            currentScope.Set(PredefinedIoCFunctions.Register,(arg) => new IoCRegisterCommand(arg[0] as string,arg[1] as Func<object[],object>));
            currentScope.Set(PredefinedScopeFunctions.SetCurrent,(arg) => new ScopesSetCurrentCommand(Scope, arg[0] as Scope));
            currentScope.Set(PredefinedScopeFunctions.CreateAndSetNew,(arg) => new ScopeCreateAndSetNewCommand());
        }

        public static T Resolve<T>(string key, params object[] args)
        {
            var func = Scope.Value.Get(key);
            return (T)func(args);
        }
    }
}