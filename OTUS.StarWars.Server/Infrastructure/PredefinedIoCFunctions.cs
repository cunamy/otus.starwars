﻿namespace OTUS.StarWars.Server.Infrastructure
{
    public class PredefinedIoCFunctions
    {
        public static string Register => "IoC.Register";
    }
}