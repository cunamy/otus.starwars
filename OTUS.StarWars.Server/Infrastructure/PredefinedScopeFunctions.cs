﻿namespace OTUS.StarWars.Server.Infrastructure
{
    public class PredefinedScopeFunctions
    {
        public static string  Current => "Scope.Current";
        public static string SetCurrent => "Scope.SetCurrent";
        public static string CreateAndSetNew => "Scope.CreateAndSetNew";
    }
}