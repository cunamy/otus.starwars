﻿using System;
using System.Collections.Generic;
using OTUS.StarWars.Server.Commands;
using OTUS.StarWars.Server.Exceptions;

namespace OTUS.StarWars.Server.Infrastructure
{
    public class Scope : IDisposable
    {
        private readonly IDictionary<string, Func<object[], object>> _dependencies;
        public string Name { get; private set; }
        public Scope(Scope parent)
        {
            _dependencies= new Dictionary<string, Func<object[], object>>();
            Parent = parent;
            Name = Guid.NewGuid().ToString();
        }

        public Func<object[], object> Get(string key)
        {
            if (_dependencies.ContainsKey(key))
            {
                return _dependencies[key];
            }

            if (Parent == null) throw new DependencyNotRegistered($"Зависимость {key} не зарегистрирована.");
            
            return Parent.Get(key);
        }

        public void Set(string key, Func<object[], object> func)
        {
            _dependencies[key] = func;
        }
        public Scope Parent { get; private set; }
        
        public void Dispose()
        {
            var func = IoC.Resolve<ICommand>(PredefinedScopeFunctions.SetCurrent,Parent);
            func.Execute();
        }
    }
}